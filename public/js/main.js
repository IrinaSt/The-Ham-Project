// let tabLinksServices = $('.service-item');
// let tabContentServices = $('.service-item-description');
// let tabServicesWrapper = $('.services-list-content');
// let ourServicesWrapper = $('.work-example-links');
// let ourWorkList = $('.work-example-title');
// let ourWorkExamples = $('.work-example-img');
// let sliderCounter = 0;
// let slider = $('.people-say-slider');
// let sliderItems = $('.people-say-item');
// let workerName = $('.people-name');
// let workerRole = $('.people-role');
// let workerWords = $('.paragraph-people-say');
// let workerPhoto = $('.people-photo-current');
// let arrow = $('.slider-arrow');
// let btnLoadMore = $('.btn-load-more');

// let workers = [
// 	{
// 		name: "Hasan Ali",
// 		role: "UX Designer",
// 		quote: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio  quis massa. Morbi pulvinar odio  quis massa. Morbi pulvinar odio  quis massa. Morbi pulvinar odio",
// 		photo: 'img/people-photos/people-photo-1.png'
// 	},

// 	{
// 		name: "Irina Stoetskaya",
// 		role: "Front-end developer",
// 		quote: "Utterly ruthless and without mercy, Zed is the leader of the Order of Shadow, an organization he created with the intent of militarizing Ionia’s magical and martial traditions to drive out Noxian invaders.",
// 		photo: 'img/people-photos/people-photo-2.png'
// 	},

// 	{
// 		name: "Master Yii",
// 		role: "THE WUJU BLADESMAN",
// 		quote: "Master Yi has tempered his body and sharpened his mind, so that thought and action have become almost as one. Though he chooses to enter into violence only as a last resort, the grace and speed of his blade ensures resolution is always swift.",
// 		photo: 'img/people-photos/people-photo-3.png'
// 	},

// 	{
// 		name: "Maokai",
// 		role: "The twisted treant",
// 		quote: "Maokai is a rageful, towering treant who fights the unnatural horrors of the Shadow Isles. He was twisted into a force of vengeance after a magical cataclysm destroyed his home, surviving undeath only through the Waters of Life infused within his heartwood.",
// 		photo: 'img/people-photos/people-photo-4.png'
// 	},

// 	{
// 		name: "Katarina",
// 		role: "Product Owner",
// 		quote: "With her people’s belief that she is the mythological hero Avarosa reincarnated, Ashe hopes to unify the Freljord once more by retaking their ancient, tribal lands.",
// 		photo: 'img/people-photos/people-photo-5.png'
// 	},
// 	{
// 		name: "Vladimir",
// 		role: "The Bloodthirster",
// 		quote: "Stoic, intelligent, and idealistic, yet uncomfortable with her role as leader, she taps into the ancestral magics of her lineage to wield a bow of True Ice.",
// 		photo: 'img/people-photos/people-photo-6.png'
// 	}
// ];

// function loadMoreContent(elem, number) {
// 	let target = $(elem).data('target');
// 	console.log(target);
// 	let targetNotActive = $(target).not('.active');
// 	targetNotActive.slice(0, number).addClass('active');
// 	targetNotActive = $(target).not('.active');
// 	if (targetNotActive.length == 0) {
// 		$(elem).fadeOut('slow');
// 	}
// };

// function showModal(elem){
// 	let target = $(elem).data('target');
// 	console.log(target);
// 	$(target).addClass('show');
// 	$('body').css('overflow', 'hidden');

// 	$( ".modal-close" ).on('click', function(){
// 	$('.modal').removeClass('show');
// 	$('body').css('overflow', 'auto');
// 		});
// }

// function showTabContent(tabLinks, tabContent, wrapper) {
// 	tabLinks.on('click', function () {
// 		tabLinks.removeClass('active');
// 		tabContent.removeClass('active');
// 		$(this).addClass('active');
// 		let category = $(this).data("category");
// 		let tab = wrapper.find('[data-category="' + category + '"]');
// 		tab.addClass('active');

// 	});
// };

// showTabContent(tabLinksServices, tabContentServices, tabServicesWrapper);
// showTabContent(ourWorkList, ourWorkExamples, ourServicesWrapper);

// $('.work-example-title-all').on('click', function () {
// 	ourWorkExamples.addClass('active');
// });

// let showImage = function (counter) {

// 	sliderItems.animate({
// 		bottom: '0'
// 	}, 300);

// 	$(sliderItems[counter]).animate({
// 		bottom: '14px'
// 	}, 1000, function () {
// 		workerName.text(workers[counter].name);
// 		workerRole.text(workers[counter].role);
// 		workerWords.text(workers[counter].quote);
// 		workerPhoto.attr('src', workers[counter].photo);
// 	});
// };

// sliderItems.on('click', function () {
// 	let counter = sliderItems.index(this);
// 	sliderCounter = counter;
// 	showImage(counter);
// })

// arrow.on('click', function () {
// 	let side = $(this).hasClass('arrow-right');

// 	if (side) {
// 		sliderCounter++;
// 		if (sliderCounter >= workers.length) {
// 			sliderCounter = 0;
// 		}
// 		let sliderItems = $('.people-say-item');
// 		sliderItems.first().appendTo(slider);

// 	}

// 	if (!side) {
// 		--sliderCounter;
// 		if (sliderCounter >= workers.length || sliderCounter <= 0) {
// 			sliderCounter = workers.length - 1;
// 		}

// 		let sliderItems = $('.people-say-item');
// 		sliderItems.last().prependTo(slider);

// 	};
// 	showImage(sliderCounter);

// });
// let grid = $('.grid');

// grid.imagesLoaded(function () {
// 	grid.masonry({
// 		columnWidth: 184,
// 		itemSelector: '.grid-item',
// 		gutter: 6
// 	});
// });

// $(".menu-top-link").on('click', function () {
// 	let target = $(this).attr('href');
// 	let targetPosition = $(target).offset().top;
// 	console.log(target);
// 	console.log(targetPosition);
// 	$("html, body").animate({ scrollTop: targetPosition }, 1000);
// });

[...document.querySelectorAll(".work-example-title")].forEach((element) => {
  element.addEventListener("click", (event) => {
    const category = event.target.dataset.category;
    [...document.querySelectorAll(".work-example-img")]
      .slice(0, 12)
      .forEach((element) => {
        element.classList.remove("active");
        if (element.dataset.category == category || !category) {
          element.classList.add("active");
        }
      });
  });
});
